# Простой калькулятор
## _Какой-то кусок кода на Python_
[![Python](https://michaelwashburnjr.com/hubfs/Imported_Blog_Media/python.jpg)](https://www.python.org)

Простой калькулятор, который считывает с пользовательского ввода три строки: первое число, второе число и операцию, после чего применяет операцию к введённым числам ("первое число" "операция" "второе число") и выводит результат на экран.

Поддерживаемые операции: +, -, /, *, mod, pow, div, где
- mod — это взятие остатка от деления
- pow — возведение в степень
- div — целочисленное деление

Если выполняется деление и второе число равно 0, необходимо выводить строку: 
> `Деление на 0!` 

## Использование
**Sample Input 1:**
```
5.0
0.0
mod
```
**Sample Output 1:**
```
Деление на 0!
```
## Текст программы
```python
# put your python code here
a = float(input())
b = float(input())
operation = input()
if (b == 0 and (operation == '/' or operation == 'mod' or operation == 'div')):
    print('Деление на 0!')
elif operation =="+":
    print(a + b)
elif operation=="-":
    print(a - b)
elif operation=="/":
    print(a / b)
elif operation=="*":
    print(a * b)
elif operation=="mod":
    print(a % b)
elif operation=="pow":
    print(a ** b)
elif operation=="div":
    print(a // b)
```


#### Процесс по курсу

Неделя | Статус задания
------------ | -------------
Первая | - [x] Циклы
Вторая |  - [x] Калькулятор
#### PS
1. Раз
2. Два
3. Три
4. Четыре
5. Пять
> `Вышел Фредди за тем кто придумал это задание!` :smile:

- [dillinger.io](https://dillinger.io) - _Людям в помощь_

